# luago

## 介绍

使用 Go 来实现 Lua 虚拟机、标准库等~

* [binchunk二进制字节码解析](./binchunk)
* [scripts测试lua脚本](./scripts)
* [vm虚拟机指令](./vm)
* [Lua API](./api)
* [Lua State](./state)
* [number数字操作](./number)

## 使用

```shell script
luac -l scripts/sum.lua
go build
./luago luac.out

``` 

## 说明

只支持 lua5.3 版本～

```go
LUAC_VERSION     = 0x53
```
