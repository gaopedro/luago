package binchunk

const (
	LUA_SIGNATURE    = "\x1bLua"
	LUAC_VERSION     = 0x53
	LUAC_FORMAT      = 0
	LUAC_DATA        = "\x19\x93\r\n\x1a\n"
	CINT_SIZE        = 4
	CSZIET_SIZE      = 8
	INSTRUCTION_SIZE = 4
	LUA_INTEGER_SIZE = 8
	LUA_NUMBER_SIZE  = 8
	LUAC_INT         = 0x5678 // 检查物理机端序是否符合
	LUAC_NUM         = 370.5  // 检查数字
)

const (
	TAG_NIL       = 0x00
	TAG_BOOLEAN   = 0x01
	TAG_NUMBER    = 0x03
	TAG_INTEGER   = 0x13
	TAG_SHORT_STR = 0x04
	TAG_LONG_STR  = 0x14
)

// 二进制 chunk 文件数据结构
type binaryChunk struct {
	header                  // 头部
	sizeUpvalues byte       // 主函数 upvalue 数量
	mainFunc     *Prototype // 主函数原型
}

// chunk 文件头部信息
type header struct {
	signature       [4]byte // 签名，魔数
	version         byte    // 版本，版本不一致直接拒绝加载
	format          byte    // 格式号，格式号不匹配则拒绝加载
	luacData        [6]byte // 校验，防止文件损坏
	cintSize        byte
	sizetSize       byte
	instructionSize byte
	luaIntegerSize  byte
	luaNumberSize   byte
	luacInt         int64
	luacNum         float64
}

// 原型
type Prototype struct {
	Source          string
	LineDefined     uint32
	LastLineDefined uint32
	NumParams       byte
	IsVararg        byte
	MaxStackSize    byte
	Code            []uint32 // 指令
	Constants       []interface{}
	Upvalues        []Upvalue
	Protos          []*Prototype
	LineInfo        []uint32
	LocVars         []LocVar
	UpvalueNames    []string
}

type Upvalue struct {
	Instack byte
	Idx     byte
}

type LocVar struct {
	VarName string
	StartPC uint32
	EndPC   uint32
}

// 解析二进制 chunk
func Undump(data []byte) *Prototype {
	reader := &reader{data}
	reader.checkHeader()        // 检查头部
	reader.readByte()           // 跳过 upvalue 数量
	return reader.readProto("") // 读取函数原型
}
