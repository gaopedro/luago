package api

type LuaVM interface {
	LuaState
	PC() int          // 返回当前 PC 值
	AddPC(n int)      // 修改 PC，指令跳转
	Fetch() uint32    // 取出指令，PC 指向下一条指令
	GetConst(idx int) // 将指令常量推入栈顶
	GetRK(rk int)     // 将指定常量或栈值推入栈顶
	RegisterCount() int
	LoadVararg(n int)
	LoadProto(idx int)
}
